#!/bin/bash
source $(dirname $0)/game_globals
#[ ! -r "$(dirname $0)/game.params.ori" ] && echo "Fichier de parameteres manquant : $(dirname $0)/game.params.ori" && exit 2
if [ ! -r "$(dirname $0)/../../params/${GAME_NAME}.params" ]; then
       echo "Mise en place du fichier de parametres dans $(dirname $0)/../../params/"
       mkdir -p $(dirname $0)/../../params || (echo "Echec de creation ou de verification du repertoire $(dirname $0)/../../params" && exit 3)
       cp $(dirname $0)/game.params.ori $(dirname $0)/../../params/${GAME_NAME}.params || (echo "Echec de generation du fichier de parametres standard $(dirname $0)/../../params/${GAME_NAME}.params" && exit 4)
       echo "Veuillez configurer le jeu si besoin via le fichier $( ls $(dirname $0)/../../params/${GAME_NAME}.params) puis relancer le script de MAJ"
       exit 0
fi
source $(dirname $0)/../../params/${GAME_NAME}.params || (echo "Impossible de charger les parametres du jeu via $(dirname $0)/../../params/${GAME_NAME}.params" && exit 5)
[ ! -d "${GAME_PATH}" ] && echo "creation du repertoire de jeu ${GAME_PATH}"
[ -z ${GAME_STEAMID} ] && "Missing App SteamID" && exit 7
echo "Mise a jour du jeu ..."
echo "${STEAMCMD} +login anonymous +force_install_dir ${GAME_PATH} +app_update ${GAME_STEAMID} +quit"
${STEAMCMD} +login anonymous +force_install_dir ${GAME_PATH} +app_update ${GAME_STEAMID} +quit
