#!/bin/bash
source $(dirname $0)/game_globals
[ ! -r "$(dirname $0)/../../params/${GAME_NAME}.params" ] && echo "Fichier de parameteres manquant : $(dirname $0)/../../params/${GAME_NAME}.params" && exit 2
source $(dirname $0)/../../params/${GAME_NAME}.params
[ ! -d ${LOG_PATH} ] && mkdir -p ${LOG_PATH}
[ ! -x ${GAME_BIN} ] && echo "Le binaire du jeu n'existe pas : ${GAME_BIN}"

echo "Lancemement du serveur..."
echo "${GAME_BIN} ${GAME_PARAMS}"
nohup ${GAME_BIN} ${GAME_PARAMS} >> ${LOG_PATH}/${GAME_NAME}_server.log 2>> ${LOG_PATH}/${GAME_NAME}_server.err &
