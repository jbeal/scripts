#!/bin/bash

# script de deploiement de machine sous linux via ssh, parceque ca me gonfle de le faire à la main tout le temps.
# le but sera de deployer les outils de base sur une becane au systeme tout neuf
# la becane aura besoin :
#   - d'un debian (ou dérivés)
#   - d'un serveur ssh (plus pratique pour deployer a distance)
#   - d'un user onimaro avec sudoers ALL=ALL(ALL) NOPASSWD:ALL en connexion mdp (on verra pour du root plus tard)

# y'a plus qu'a espérer que ce script ne reste pas 7 lignes de commentaire durant toute son existance.

# parametres 
# param1 : machine_cible : url ou ip de la machine sur laquelle on souhaite déployer le bordel

##################################
# help
if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "" ]
then
  echo
  echo "###########################################################################################################################"
  echo "Syntaxe : ${0##*/} machine_cible"
  echo "machine_cible = url ou ip de la machine cible"
  echo "###########################################################################################################################"
  echo
  exit 0
fi

##################################
# check nombre d'arguments
if [ $# -ne 1 ] 
then
  echo "Mauvais nombre d'arguments : $#"
  echo "Il en faut 1"
  exit 1
fi

# constantes a passer en fichier de parametres
user_cible=onimaro

# recuperation des arguments
machine_cible=$1



##################################
#            FONCTIONS           #
##################################
# fonction connexionSSH
# arguments :
# arg1 : commande lancée
#
# passage de commandes vers la cible en SSH
#

function connexionSSH
{
  # check nombre d'arguments
  if [ $# -ne 1 ] 
  then
    echo "Mauvais nombre d'arguments : $#"
    echo "Il en faut 1"
    exit 1
  fi

  # recuperation arguments
  commande_cible=$1
  
  [ $(ssh ${user_cible}@${machine_cible} "${commande_cible}") -eq 0 ] || ( echo erreur sur la commande ${commande_cible} && exit 4 )
}

# fonction disaster_hunter
# arguments :
# aucun
# 
# on anticipe diverses catastrophes
function disaster_hunter
{
  # est-ce que le premier argument n'est pas une injection ?
  [ $(echo ${machine_cible} | wc -w) -eq 1 ] || ( echo "une url ou une IP, rien d'autre, SVP" && exit 2 )

  # est-ce qu'on se connecte à la machine en SSH ?
  [ $(ssh ${machine_cible} "exit") -eq 0 ] || ( echo "machine ${machine_cible} injoignable." && exit 3 )

  # verification des listes de paquets et de users
  for fichier in liste_paquets liste_users; do
    if [ -r ${fichier} ]; then
        OLD_IFS=$IFS
        IFS='\n'
        for paquet in $(cat ${fichier}); do
            [ $(echo ${paquet} | wc -w ) -eq 1 ] || shithappened=8
        done
        IFS=${OLD_IFS}
        [ ${shithappened} -eq 8 ] && echo "Le fichier ${fichier} ne doit contenir qu'un mot par ligne" && exit 6
        pas_de_${fichier}=0
    else
        echo "fichier ${fichier} illisible ou introuvable."
        pas_de_${fichier}=1
    fi
  done
}

# fonction deploy_packages
# arguments :
# aucun
# 
# on deploie les paquets a partir du fichier de parametres liste_paquets
function deploy_packages
{
  for paquet in $(cat ${fichier_paquets}); do
    connexionSSH ${paquet}
  done
}




##################################
#              MAIN              #
##################################
disaster_hunter
[ ${pas_de_liste_paquets} -eq 0 ] && deploy_packages
[ ${pas_de_liste_users} -eq 0 ] && deploy_packages
deploy_users
