#!/bin/bash

serveur=$1
shard=$2

[ ! -r "$(dirname $0)/../../params/${GAME_NAME}.params" ] && echo "Fichier de parameteres manquant : $(dirname $0)/../../params/${GAME_NAME}.params" && exit 2
source $(dirname $0)/../../params/${GAME_NAME}.params

([ -z ${serveur} ] && echo "XXX Missing parameter : server name XXX" && exit 1) || echo "Serveur : ${serveur}"
([ -z ${shard} ] && echo "XXX Missing parameter : shard [dehors|caves] XXX" && exit 1) || echo "Shard : ${shard}"
[ ${shard} != "Master" ] && [ ${shard} != "Caves" ] && echo "XXX Parameter error : shard [dehors|caves] XXX" && exit 1
mkdir -p ${LOG_PATH}
nohup ${GAME_BIN} ${GAME_PARAMS} >> ${LOG_PATH}/${serveur}_${shard}.out 2>>${LOG_PATH}/${serveur}_${shard}.err &
